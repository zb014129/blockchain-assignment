﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    {
        // Global blockchain object
        private Blockchain blockchain;
        private int miningSettingIndex = 0; //Mining Setting index determines which mining setting will be used during genetating a block
        //0 will be random
        //1 will be Greedy and 2 will be alturuistic method
        public static int blockchainDifficulty = 4;
        // Default App Constructor
        public BlockchainApp()
        {

            // Initialise UI Components
            InitializeComponent();
            // Create a new blockchain 
            blockchain = new Blockchain();
            // Update UI with an initalisation message
            UpdateText("New blockchain initialised!");
        }

        /* PRINTING */
        // Helper method to update the UI with a provided message
        private void UpdateText(String text)
        {
            output.Text = text;
        }

        // Print entire blockchain to UI
        private void ReadAll_Click(object sender, EventArgs e)
        {
            UpdateText(blockchain.ToString());
        }

        // Print Block N (based on user input)
        private void PrintBlock_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse(blockNo.Text, out int index))
                UpdateText(blockchain.GetBlockAsString(index));
            else
                UpdateText("Invalid Block No.");
        }

        // Print pending transactions from the transaction pool to the UI
        private void PrintPendingTransactions_Click(object sender, EventArgs e)
        {
            UpdateText(String.Join("\n", blockchain.transactionPool));
        }

        /* WALLETS */
        // Generate a new Wallet and fill the public and private key fields of the UI
        private void GenerateWallet_Click(object sender, EventArgs e)
        {
            Wallet.Wallet myNewWallet = new Wallet.Wallet(out string privKey);

            publicKey.Text = myNewWallet.publicID;
            privateKey.Text = privKey;
        }

        // Validate the keys loaded in the UI by comparing their mathematical relationship
        private void ValidateKeys_Click(object sender, EventArgs e)
        {
            if (Wallet.Wallet.ValidatePrivateKey(privateKey.Text, publicKey.Text))
                UpdateText("Keys are valid");
            else
                UpdateText("Keys are invalid");
        }

        // Check the balance of current user
        private void CheckBalance_Click(object sender, EventArgs e)
        {
            UpdateText(blockchain.GetBalance(publicKey.Text).ToString() + " Assignment Coin");
        }


        /* TRANSACTION MANAGEMENT */
        // Create a new pending transaction and add it to the transaction pool
        private void CreateTransaction_Click(object sender, EventArgs e)
        {
            Transaction transaction = new Transaction(publicKey.Text, reciever.Text, Double.Parse(amount.Text), Double.Parse(fee.Text), privateKey.Text);
            /* TODO: Validate transaction */
            blockchain.transactionPool.Add(transaction);
            UpdateText(transaction.ToString());
        }

        /* BLOCK MANAGEMENT */
        // Conduct Proof-of-work in order to mine transactions from the pool and submit a new block to the Blockchain
        private void NewBlock_Click(object sender, EventArgs e)
        {   
            createNewBlock();
            
            
        }
        /**
         * Returns transaction List in descending order for greedy mining setting
         */
        private  List<Transaction> greedySetting(List<Transaction> transactionList){
            transactionList.Sort(delegate (Transaction x, Transaction y)
            {
                return y.getFee().CompareTo(x.getFee());
            });
            return transactionList;
        }
        /**
         * Returns transactionList based on the oldest transaction(i.e waiting the longest)
         */
        private List<Transaction>altruisticSetting(List<Transaction> transactionList)
        {
            transactionList.Sort(delegate (Transaction x, Transaction y)
            {
                return x.getTime().CompareTo(y.getTime());
            });
            return transactionList;
        }

        private void createNewBlock()
        {
            // Retrieve pending transactions to be added to the newly generated Block
            List<Transaction> transactions = blockchain.GetPendingTransactions();

            //Checks if greedy transaction setting is being used
            if (miningSettingIndex == 1)
            {
                //Sort Transaction order list
                transactions = greedySetting(transactions);//Sorts transaction

            }

            for(int i = 0; i<transactions.Count; i++)
            {
                Console.WriteLine("Time is : " + transactions[i].getTime());
            }


            if (miningSettingIndex == 2)
            {
                transactions = altruisticSetting(transactions);
            }


            Console.WriteLine("After sorting time data");
            for (int i = 0; i < transactions.Count; i++)
            {
                Console.WriteLine("Time is : " + transactions[i].getTime());
            }





            // Create and append the new block - requires a reference to the previous block, a set of transactions and the miners public address (For the reward to be issued)
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text);
            blockchain.blocks.Add(newBlock);

            UpdateText(blockchain.ToString());
        }


        /* BLOCKCHAIN VALIDATION */
        // Validate the integrity of the state of the Blockchain
        private void Validate_Click(object sender, EventArgs e)
        {
            // CASE: Genesis Block - Check only hash as no transactions are currently present
            if(blockchain.blocks.Count == 1)
            {
                if (!Blockchain.ValidateHash(blockchain.blocks[0])) // Recompute Hash to check validity
                    UpdateText("Blockchain is invalid");
                else
                    UpdateText("Blockchain is valid");
                return;
            }

            for (int i=1; i<blockchain.blocks.Count-1; i++)
            {
                if(
                    blockchain.blocks[i].prevHash != blockchain.blocks[i - 1].hash || // Check hash "chain"
                    !Blockchain.ValidateHash(blockchain.blocks[i]) ||  // Check each blocks hash
                    !Blockchain.ValidateMerkleRoot(blockchain.blocks[i]) // Check transaction integrity using Merkle Root
                )
                {
                    UpdateText("Blockchain is invalid");
                    return;
                }
            }
            UpdateText("Blockchain is valid");
        }

        //Method called when the selected Index is changed in mine setting drop box
        private void setting_SelectedIndexChanged(object sender, EventArgs e)
        {
            miningSettingIndex = setting.SelectedIndex;
            output.Text = "We have changed the Mining Setting to" + miningSettingIndex;
            
        }

        public static int getDifficulty()
        {
            return blockchainDifficulty;
        }
    }
}